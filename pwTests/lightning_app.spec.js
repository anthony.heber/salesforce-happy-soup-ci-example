const { test } = require('@playwright/test');
const { getOrgAccessUrl } = require('../scripts/urlBuilderUtils');

const ORG_BASE_URL = getOrgAccessUrl(
    'scratch',
    '/lightning/n/Product_Explorer'
);

test('Lightning app', async ({ page }) => {
    await page.goto(await ORG_BASE_URL);
    // wait for the components to load
    // wait find one of the pieces of the E-Bikes listing
    await page.waitForSelector('text=DYNAMO X4');
    // click one of the pices of the E-Bikes listing
    await page.click('text=DYNAMO X4');
    // ensure the E-Bike is loaded into the other component
    await page.waitForSelector('c-product-card span:has-text("DYNAMO X4")');
});
