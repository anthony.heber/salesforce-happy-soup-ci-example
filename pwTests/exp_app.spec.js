const { test } = require('@playwright/test');
const { getExperienceAccessURL } = require('../scripts/urlBuilderUtils');

const EXPERIENCE_BASE_URL = getExperienceAccessURL('scratch', 'E-Bikes');

test('Lightning app', async ({ page }) => {
    await page.goto(await EXPERIENCE_BASE_URL);
    // wait for the components to load
    // wait find one of the pieces of the E-Bikes listing
    await page.waitForSelector('c-hero-details span:has-text("Explore More")');
});
