const config = {
    testDir: 'pwTests',
    retries: 3,
    timeout: 60000,
    use: {
        headless: true,
        ignoreHTTPSErrors: true,
        video: 'on-first-retry'
    }
};
module.exports = config;
