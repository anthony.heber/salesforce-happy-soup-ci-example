const xml2js = require('xml2js');
const fs = require('fs');

function trimPackage(config) {
    let parser = new xml2js.Parser();
    let cType = 'keep';
    if (config.change[0] == '-') {
        cType = 'remove';
        config.change = config.change.slice(1);
    }
    let types = config.change.split(',');
    const data = fs.readFileSync(config.file);
    parser.parseString(data, (err, result) => {
        if (err) {
            console.error(err);
        }
        let nt = [];
        result.Package.types.forEach((t) => {
            let hasType = types.includes(t.name[0]);
            if (
                (cType == 'keep' && hasType === true) ||
                (cType == 'remove' && hasType === false)
            ) {
                nt.push(t);
            }
        });
        result.Package.types = nt;
        let builder = new xml2js.Builder();
        let xml = builder.buildObject(result);
        fs.writeFileSync(config.file, xml);
    });
}
module.exports = trimPackage;
if (require.main == module) {
    const config = {};
    config.file = process.argv[2];
    config.change = process.argv[3];
    trimPackage(config);
}
