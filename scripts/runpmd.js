const { exec } = require('child_process');
const os = require('os');
const path = require('path');
const fs = require('fs');
// Slice off script name and args
var args = process.argv.slice(2);
const TEMP_FILE = path.join(os.tmpdir(), 'pmdtmplistforgit.txt');

// take all classes passed to ARGS and stick them in a file for PMD to analyze
const rootDir = getRootDir();
let files = args
    .filter((f) => fs.existsSync(path.join(rootDir, f))) // make sure each file exists
    .map((f) => path.join(rootDir, f)); // get a fully qualified path

if (files.length === 0) {
    process.exit(0); // no files, call it good
}

fs.writeFileSync(TEMP_FILE, files.join('\n')); // write to the temp file

// If you want to run on your own PMD config file, swap these lines
// let pmdCommand = `pmd -f text -R config/pmd-apex-ruleset.xml -filelist "${TEMP_FILE}"`; // build PMD string to run against the temp file
let pmdCommand = `pmd -f text -filelist "${TEMP_FILE}"`; // build PMD string to run against the temp file
if (process.platform === 'darwin') {
    pmdCommand = `pmd ${pmdCommand}`; // Mac invokes PMD a little differently
}
const pmd = exec(pmdCommand);

// logging stdout passthrough
pmd.stdout.on('data', function (data) {
    console.log(data);
});

// logging stderr passthrough
pmd.stderr.on('data', function (data) {
    // Suppressing console error lines because it cleaned up the data significantly
    // console.error(data);
});

// once process is all done, capture error code giving priority to the furthest
// from zero as the code to return
pmd.on('close', function (code) {
    fs.unlinkSync(TEMP_FILE);
    process.exit(code);
});

function getRootDir() {
    return path.dirname(__dirname);
}
