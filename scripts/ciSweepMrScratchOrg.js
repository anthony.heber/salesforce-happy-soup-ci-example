const orgUtils = require('./ciVendingMachineUtils');

async function run() {
    console.log('Retrieving org for', process.env.CI_MERGE_REQUEST_REF_PATH);
    const org = await orgUtils.getOrg(process.env.CI_MERGE_REQUEST_REF_PATH);
    const sweepOutput = await orgUtils.sweepOrg(org.id);
    console.log(sweepOutput);
}

run().catch((error) => {
    console.error(error);
    process.exit(1);
});
