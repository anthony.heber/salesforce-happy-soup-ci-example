require('dotenv').config();
var dircompare = require('dir-compare'),
    format = require('util').format,
    fs = require('fs');

// Map is used to map mdapi type directories to the Type-> Name in the destructiveChanges.xml as well as
// serves to enable specific metadata types for automatic deletion
const sobjectMap = {
    classes: 'ApexClass',
    aura: 'AuraDefinitionBundle',
    triggers: 'ApexTrigger',
    staticresources: 'StaticResource',
    pages: 'ApexPage'
};

// List files present in the org and missing from the src
// var options = {compareContent: true};
var options = {
    compareContent: true,
    compareFileSync:
        dircompare.fileCompareHandlers.lineBasedFileCompare.compareSync,
    compareFileAsync:
        dircompare.fileCompareHandlers.lineBasedFileCompare.compareAsync,
    ignoreLineEnding: true,
    ignoreWhiteSpaces: true,
    excludeFilter:
        '*.objectTranslation,et4ae5__*,*.profile,*.quickAction,*.topicsForObjects,standard__*.app,Quick_Link*.md,layouts,certs,datacategorygroups,emailservices,reportTypes,Case.settings,Knowledge.settings,OrgPreference.settings'
};
// Allow environment variables to suppress deleting specific components
// This is because despite a desire to delete some things we've found that
// Communities hold onto memories of things and we need a quick method to add exclusions
if (process.env.CI_EXCLUDE_FROM_DESTRUCTIVE_CHANGES) {
    options.excludeFilter +=
        ',' + process.env.CI_EXCLUDE_FROM_DESTRUCTIVE_CHANGES;
}
console.log('Exclusion Filter Used:', options.excludeFilter);

var path1 = './dist';
var path2 = './orgmdt';

var states = { equal: '==', left: '->', right: '<-', distinct: '<>' };

// Synchronous
var res = dircompare.compareSync(path1, path2, options);

let missingFiles = [];
let fileDiffs = [];
let objectFiles = [];
let workflowFiles = [];
let labelFiles = [];

res.diffSet.forEach(function (entry) {
    // Exists in target org but not in source
    // Will not correctly report missing fields in this format
    if (entry.type2 === 'missing' || entry.type2 === 'directory') {
        return;
    }
    if (entry.type1 === 'missing') {
        var name2 = entry.name2 ? entry.name2 : '';
        missingFiles.push(entry.path2.replace(/.*orgmdt.?/, '') + '/' + name2);
        // console.log(format('Entry not tracked: %s', name2));
    } else {
        if (entry.state != 'equal') {
            var state = states[entry.state];
            var name1 = entry.name1 ? entry.name1 : '';
            var name2 = entry.name2 ? entry.name2 : '';

            if (name1.endsWith('.object')) {
                objectFiles.push(name2);
            }
            if (name1.endsWith('.workflow')) {
                workflowFiles.push(name2);
            }
            if (name1.endsWith('.labels')) {
                labelFiles.push(name2);
            }

            fileDiffs.push(
                format(
                    '%s(%s)%s%s(%s)',
                    name1,
                    entry.type1,
                    state,
                    name2,
                    entry.type2
                )
            );
        }
    }
});

buildDestructiveChanges(missingFiles);

function buildDestructiveChanges(missingFiles) {
    let data = {};
    missingFiles.forEach((f) => {
        const parts = f.split(/[\\/]/);
        const mtdType = sobjectMap[parts[0]];
        if (mtdType) {
            if (data[mtdType] === undefined) {
                data[mtdType] = {};
            }
            let configName = parts[1].split('.')[0];
            data[mtdType][configName] = true;
        }
    });
    // console.log(data);
    const destructiveChangesOutput = buildFile(data);
    fs.writeFileSync(
        'dist/destructiveChangesPost.xml',
        destructiveChangesOutput
    );
}

function buildFile(data) {
    return `<?xml version="1.0" encoding="UTF-8"?>
<Package xmlns="http://soap.sforce.com/2006/04/metadata">${buildTypes(data)}
</Package>
`;
}

function buildTypes(data) {
    let rVal = '';
    Object.keys(data).forEach((k) => {
        rVal += buildType(k, data[k]);
    });
    return rVal;
}

function buildType(mdType, data) {
    return `\n    <types>
${Object.keys(data)
    .map((m) => {
        return `      <members>${m}</members>`;
    })
    .join('\n')}
      <name>${mdType}</name>
    </types>`;
}
