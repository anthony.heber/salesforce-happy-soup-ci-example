const orgUtils = require('./ciVendingMachineUtils');
const publishAllCommunities = require('./publishAllCommunities');
const sfdx = require('./sfdx');
const createScratchOrg = require('./createScratchOrg');

async function run() {
    let org = await orgUtils.checkWithCoordinator(
        process.env.CI_MERGE_REQUEST_REF_PATH
    );
    console.log('ORG', org);
    if (org != undefined) {
        await orgUtils.authOrg(org.username);
        await sfdx(`force:source:push -u ${org.username} -f`);
        await publishAllCommunities(org.username);
    } else {
        org = await createScratchOrg();
    }

    await orgUtils.registerWithCoordinator(
        process.env.CI_MERGE_REQUEST_REF_PATH,
        org.username
    );
}

run().catch((error) => {
    console.error(error);
    process.exit(1);
});
