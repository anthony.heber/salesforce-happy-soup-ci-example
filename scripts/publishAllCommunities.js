const sfdx = require('./sfdx');

async function publishCommunities(username) {
    let u = username;
    if (u === undefined || u.length == 0) {
        u = process.env.CI_SFDX_SCRATCH_ORG_ALIAS;

        if (u === undefined || u.length == 0) {
            u = 'scratch';
        }
    }
    // get list of communities from org
    // Using OLD single-threaded publishing due to bug found in concurrent publishing
    // Will revert when the bug has been fixed
    let data = JSON.parse(
        (
            await sfdx(
                'force:data:soql:query',
                `--targetusername ${u} -q "select id, Name, status, UrlPathPrefix, OptionsSiteAsContainerEnabled from Network WHERE OptionsSiteAsContainerEnabled = TRUE" --json`
            )
        ).stdout
    );
    if (data.status !== 0) {
        throw Error('Error retrieving network list');
    }
    // loop through each community and publish using SFDX plugin
    for (const c of data.result.records) {
        console.log(`Publishing ${c.Name} Community`);
        await sfdx(
            'force:community:publish',
            `--targetusername ${u} --name "${c.Name}"`
        );
        // To avoid cross-contaminating communities. I've introduced an arbitrary delay
        await sleep(5 * 1000);
    }
}
function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}
module.exports = publishCommunities;
if (require.main == module) {
    let username = process.argv[2];
    require('dotenv').config();
    publishCommunities(username);
}
