const sfdx = require('./sfdx');

async function run() {
    const ORG_ALIAS = 'scratch';

    console.log(`Installing E-Bikes scratch org (${ORG_ALIAS})\n`);

    console.log('Creating scratch org...\n');
    await sfdx(
        'force:org:create',
        `-s -f config/project-scratch-def.json -d 30 -a ${ORG_ALIAS}`
    );

    console.log('Pushing source...\n');
    await sfdx('force:source:push', `-u ${ORG_ALIAS}`);

    console.log('Assigning permission sets...\n');
    await sfdx('force:user:permset:assign', `-n ebikes`);

    console.log('Importing sample data...');
    await sfdx('force:data:tree:import', `-p data/sample-data-plan.json`);

    console.log('Sleeping 30s for Community deployment');
    // sleep 30

    console.log('Publishing Community');
    await sfdx('force:community:publish', '-n E-Bikes');

    console.log('Opening org...');
    await sfdx('force:org:open', '-p lightning/n/Product_Explorer');

    return JSON.parse(
        (await sfdx('force:org:display', `-u ${ORG_ALIAS} --json`)).stdout
    ).result;
}

module.exports = run;
