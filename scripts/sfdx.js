const { exec } = require('child_process');
const path = require('path');

function getRootDir() {
    return path.dirname(__dirname);
}

async function sfdx(command, args = '', options = {}) {
    return new Promise((resolve, reject) => {
        const commandString = 'sfdx ' + command + ' ' + args;

        let mergedOptions = Object.assign(
            { timeout: 3600000, cwd: getRootDir(), maxBuffer: Infinity },
            options
        );

        // Merge submitted environment variables with existing process variables
        mergedOptions.env = Object.assign(process.env, mergedOptions.env);

        if (
            !process.env.CI_SFDX_SUPRESS_TIMESTAMP_LOGGING ||
            process.env.CI_SFDX_SUPRESS_TIMESTAMP_LOGGING.toLowerCase() !==
                'true'
        ) {
            console.log('    Starting:', commandString);
        }
        const startTime = new Date();
        exec(commandString, mergedOptions, (err, stdout, stderr) => {
            if (
                !process.env.CI_SFDX_SUPRESS_TIMESTAMP_LOGGING ||
                process.env.CI_SFDX_SUPRESS_TIMESTAMP_LOGGING.toLowerCase() !==
                    'true'
            ) {
                console.log(
                    '    Finished:',
                    commandString,
                    'in',
                    msToTime(new Date() - startTime)
                );
            }
            if (err) {
                reject({ status: 1, err: err, stdout, stderr });
                return;
            }
            resolve({ status: 0, err: err, stdout, stderr });
        });
    });
}

// https://coderwall.com/p/wkdefg/converting-milliseconds-to-hh-mm-ss-mmm
function msToTime(duration) {
    var milliseconds = parseInt((duration % 1000) / 100),
        seconds = parseInt((duration / 1000) % 60),
        minutes = parseInt((duration / (1000 * 60)) % 60),
        hours = parseInt((duration / (1000 * 60 * 60)) % 24);

    hours = hours < 10 ? '0' + hours : hours;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    seconds = seconds < 10 ? '0' + seconds : seconds;

    return hours + ':' + minutes + ':' + seconds + '.' + milliseconds;
}
module.exports = sfdx;
