const fs = require('fs-extra');
const { promisify } = require('util');
const glob = promisify(require('glob'));

function getEnvVar(varname) {
    return process.env[varname] || '';
}

async function listApexTestClasses({ testFolder, totalShards, shardNumber }) {
    let globString;
    if (testFolder) {
        globString = `force-app/${testFolder}/**/*.cls`;
    } else {
        globString = `force-app/**/*.cls`;
    }
    // look for all apex classes matching our filters
    const files = await glob(globString);
    // for each discovered apex class, read its contents and determine if it includes any tests
    let classes = [];
    const promises = [];
    files.forEach((f) => {
        promises.push(
            fs.promises
                .readFile(f)
                .then((data) => data.toString())
                .then((fContent) => {
                    if (fContent.toLowerCase().includes('@istest')) {
                        classes.push(f.replace(/.*\/(.*).cls/, '$1'));
                    }
                })
        );
    });

    await Promise.all(promises);

    // No test classes in this folder, moving on
    if (classes.length === 0) {
        return [];
    }
    classes.sort();
    // If we are sharding tests then select only the specific tests we're assigned to run
    if (shardNumber && shardNumber.length > 0) {
        const shardNum = parseInt(shardNumber, 10);
        const shardCount = parseInt(totalShards, 10);
        let newClasses = [];
        for (let i = shardNum - 1; i < classes.length; i = i + shardCount) {
            newClasses.push(classes[i]);
        }
        classes = newClasses;
    }
    return classes;
}

module.exports = {
    getEnvVar,
    listApexTestClasses
};
