const https = require('https');
const sfdx = require('./sfdx');

async function authOrg(username, authUrl = 'https://test.salesforce.com') {
    return await sfdx(
        'force:auth:jwt:grant',
        `--clientid ${process.env.CI_SFDX_CONSUMER_KEY} --jwtkeyfile="${process.env.CI_PROJECT_DIR}/assets/server.key" --username ${username} --setalias="scratch" --instanceurl="${authUrl}" --setdefaultusername`
    );
}

async function checkWithCoordinator(slug) {
    return new Promise((resolve, reject) => {
        let data = '';
        const options = getHttpRequestOptions();
        const p =
            '/api/v1/org' + (!!slug ? `?slug=${encodeURIComponent(slug)}` : '');
        options.path = p;
        options.method = 'GET';

        const req = https.request(options, (res) => {
            res.on('data', (d) => {
                data += d;
            });

            res.on('end', () => {
                try {
                    resolve(JSON.parse(data));
                } catch (error) {
                    resolve(undefined);
                }
            });
        });

        req.on('error', (error) => {
            reject(error);
        });

        req.end();
    });
}
async function registerWithCoordinator(slug, username) {
    return new Promise((resolve, reject) => {
        let data = '';
        const options = getHttpRequestOptions();
        options.path = '/api/v1/org';
        options.method = 'POST';
        options.headers = { 'Content-Type': 'application/json' };

        const req = https.request(options, (res) => {
            res.on('data', (d) => {
                data += d;
            });

            res.on('end', () => {
                resolve(JSON.parse(data));
            });
        });

        req.on('error', (error) => {
            reject(error);
        });

        req.write(JSON.stringify({ slug, username }));
        req.end();
    });
}

function getHttpRequestOptions() {
    return {
        hostname: 'temp-sfdc-ci-help.herokuapp.com',
        port: 443,
        method: 'GET'
    };
}

module.exports = { authOrg, registerWithCoordinator, checkWithCoordinator };
