'use strict';
require('dotenv').config();

// Overwrite the log statements to provide timestamps
var origlog = console.log;

console.log = function (obj, ...placeholders) {
    if (typeof obj === 'string')
        placeholders.unshift(new Date().toJSON() + ' -- ' + obj);
    else {
        // This handles console.log( object )
        placeholders.unshift(obj);
        placeholders.unshift(new Date().toJSON() + ' -- %j');
    }

    origlog.apply(this, placeholders);
};
