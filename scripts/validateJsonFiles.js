const { promisify } = require('util');
const fs = require('fs-extra');
const glob = promisify(require('glob'));

async function run() {
    const files = await glob(`{,!(node_modules)/**/}*.json`);
    const promises = [];
    for (const f of files) {
        promises.push(verifyFile(f));
    }
    if (
        (await Promise.allSettled(promises)).some(
            (p) => p.status === 'rejected'
        )
    ) {
        throw 'Some JSON files failed validation';
    }
}

async function verifyFile(file) {
    try {
        const content = await fs.promises.readFile(file, 'utf-8');
        JSON.parse(content);
    } catch (error) {
        console.error(file + '::' + error);
        throw error;
    }
}

run().catch((error) => {
    console.error(error);
    process.exit(1);
});
