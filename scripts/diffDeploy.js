require('./scriptCommon');
const fs = require('fs-extra');
const sfdx = require('./sfdx');
const { promisify } = require('util');
const glob = promisify(require('glob'));
const exec = promisify(require('child_process').exec);
const trimPackage = require('./trimPackage');
const publishAllCommunities = require('./publishAllCommunities');

async function run() {
    let promises = [];

    console.log('Copy .forceignore-deploy from config');
    // Many aspects of source-prep before converting to MDAPI format
    promises.push(
        Promise.all([
            fs.promises
                .unlink(`${getEnvVar('CI_PROJECT_DIR')}/.forceignore`)
                .then(() => {
                    fs.promises.copyFile(
                        `${getEnvVar(
                            'CI_PROJECT_DIR'
                        )}/config/.forceignore-deploy`,
                        `${getEnvVar('CI_PROJECT_DIR')}/.forceignore`
                    );
                }),
            // rewrite workflow email alerts to use the correct email address for the store-bought orgs
            sfdx('devops:workflow:emailalert:replaceaddress').catch((error) => {
                console.error(error); // TODO: handle this correctly
            })
        ]).then(() => {
            // All the local source prep work is done, convert to MDT format now
            return sfdx('force:source:convert', `--outputdir dist`);
        })
    );

    // Get a handle of the org then push/pull data
    promises.push(
        sfdx(
            'force:auth:jwt:grant',
            `--clientid "${getEnvVar(
                'CI_SFDX_CONSUMER_KEY'
            )}" --jwtkeyfile "${getEnvVar(
                'CI_PROJECT_DIR'
            )}/assets/server.key" --username "${getEnvVar(
                'CI_SFDX_ORG_USERNAME'
            )}" --instanceurl "${getEnvVar('CI_SFDX_INSTANCEURL')}"`
        ).then(() => {
            return Promise.all([
                sfdx(
                    'shane:data:file:download',
                    `--targetusername ${getEnvVar(
                        'CI_SFDX_ORG_USERNAME'
                    )} -n unpackaged -o orgmdt`
                ).catch(() => {
                    return sfdx(
                        'sfpowerkit:org:manifest:build',
                        `--targetusername ${getEnvVar(
                            'CI_SFDX_ORG_USERNAME'
                        )} --outputfile manifest/package.xml --excludemanaged`
                    ).then(() => {
                        // CustomIndex metadata type is causing package generation faulure
                        // sfpowerkit should be able to exclude but quickfilter flag isn't working right
                        // https://github.com/Accenture/sfpowerkit/issues/424
                        trimPackage({
                            file: 'manifest/package.xml',
                            change: '-CustomIndex'
                        });
                        return sfdx(
                            'force:mdapi:retrieve',
                            `--targetusername ${getEnvVar(
                                'CI_SFDX_ORG_USERNAME'
                            )} --unpackaged manifest/package.xml --wait 30 --singlepackage --retrievetargetdir orgmdt --json`
                        );
                    });
                })
            ]);
        })
    );

    await Promise.all(promises);

    console.log('unzip orgmdt/unpackaged.zip');
    try {
        const { stdout, stderr } = await exec(
            'unzip -qq orgmdt/unpackaged.zip -d orgmdt'
        );
        console.log(stdout);
        console.log(stderr);
    } catch (e) {
        console.error(e);
        process.exit(e.code);
    }

    console.log('Delete orgmdt/unpackaged.zip');
    await fs.promises.unlink('orgmdt/unpackaged.zip');

    console.log('Build destructive changes');
    require('./buildDestructiveChanges');

    await sfdx(
        'devops:mdsource:compare:build',
        `--basedir dist --changeddir orgmdt --outputdir distDiff`
    );

    console.log('distDiff/package.xml');
    console.log(
        (await fs.promises.readFile('distDiff/package.xml')).toString()
    );

    console.log('distDiff/destructiveChangesPost.xml');
    console.log(
        (
            await fs.promises.readFile('distDiff/destructiveChangesPost.xml')
        ).toString()
    );

    let testFolder = getEnvVar('CI_SFDX_TESTS_FOLDER');
    let testNum = getEnvVar('CI_SFDX_SB_ORG_NUM');
    let runWithOutTests = getEnvVar('CI_SFDX_DEPLOY_WITHOUT_TESTS');
    if (
        (testFolder && testFolder.length > 0) ||
        (testNum && testNum.length > 0)
    ) {
        await deployWithPartialTests({ testFolder, testNum });
    } else if (runWithOutTests && runWithOutTests.toLowerCase() === 'true') {
        try {
            await sfdx(
                'force:mdapi:deploy',
                `--deploydir distDiff --testlevel NoTestRun --targetusername ${getEnvVar(
                    'CI_SFDX_ORG_USERNAME'
                )} ${getEnvVar('CI_SFDX_MDAPIDEPLOY_ARGS')} --wait 60 --json`
            );
        } catch (error) {
            printDeployFailures(JSON.parse(o.stdout));
            throw new Error('Deployment Failed');
        }
    } else {
        try {
            await sfdx(
                'force:mdapi:deploy',
                `--deploydir distDiff --testlevel RunLocalTests --targetusername ${getEnvVar(
                    'CI_SFDX_ORG_USERNAME'
                )} ${getEnvVar('CI_SFDX_MDAPIDEPLOY_ARGS')} --wait 60 --json`
            );
        } catch (o) {
            printDeployFailures(JSON.parse(o.stdout));
            throw new Error('Deployment Failed');
        }
    }

    let publishPromise;
    if (process.env.CI_SFDX_PUBLISH_COMMS === 'true') {
        publishPromise = publishAllCommunities(
            getEnvVar('CI_SFDX_ORG_USERNAME')
        );
    }

    // If configured, try and upload the package to the org during the deploy phase so we can re-use it for the validation runs
    if (getEnvVar('CI_SFDX_UPLOAD_PACKAGE') === 'true') {
        const packageBuildPromise = sfdx(
            'sfpowerkit:org:manifest:build',
            `--targetusername ${getEnvVar(
                'CI_SFDX_ORG_USERNAME'
            )} --outputfile manifest/package.xml --excludemanaged`
        ).then(() => {
            // CustomIndex metadata type is causing package generation faulure
            // sfpowerkit should be able to exclude but quickfilter flag isn't working right
            // https://github.com/Accenture/sfpowerkit/issues/424
            trimPackage({
                file: 'manifest/package.xml',
                change: '-CustomIndex'
            });
        });

        const output = JSON.parse(
            (
                await sfdx(
                    'force:data:soql:query',
                    `--targetusername ${getEnvVar(
                        'CI_SFDX_ORG_USERNAME'
                    )} --query "SELECT Id FROM ContentDocument WHERE Title='unpackaged' AND FileType = 'ZIP' AND FileExtension='zip'" --json`
                )
            ).stdout
        );
        const promises = [packageBuildPromise];
        for (const f of output.result.records) {
            promises.push(
                sfdx(
                    'force:data:record:delete',
                    `--targetusername ${getEnvVar(
                        'CI_SFDX_ORG_USERNAME'
                    )} --sobjecttype ContentDocument --sobjectid "${f.Id}"`
                )
            );
        }
        await Promise.all(promises);

        await sfdx(
            'force:mdapi:retrieve',
            `--targetusername ${getEnvVar(
                'CI_SFDX_ORG_USERNAME'
            )} --unpackaged manifest/package.xml --wait 30 --singlepackage --retrievetargetdir orgmdt`
        );
        await sfdx(
            'shane:data:file:upload',
            `--targetusername ${getEnvVar(
                'CI_SFDX_ORG_USERNAME'
            )} --file orgmdt/unpackaged.zip`
        );
    }
    // ensure the communit publish process has completed
    if (publishPromise) {
        await publishPromise;
    }
}

function getEnvVar(varname) {
    return process.env[varname] || '';
}

run().catch((err) => {
    console.log('Error:', err);
    process.exit(1);
});

// run deployment with only a subset of tests
// error handling custom tailored to accept coverage failures
async function deployWithPartialTests({ testFolder, testNum }) {
    let globString;
    if (testFolder) {
        globString = `force-app/${testFolder}/**/*.cls`;
    } else {
        globString = `force-app/**/*.cls`;
    }
    // look for all apex classes matching our filters
    const files = await glob(globString);
    // for each discovered apex class, read its contents and determine if it includes any tests
    let classes = [];
    const promises = [];
    files.forEach((f) => {
        promises.push(
            fs.promises
                .readFile(f)
                .then((data) => data.toString())
                .then((fContent) => {
                    if (fContent.toLowerCase().includes('@istest')) {
                        classes.push(f.replace(/.*\/(.*).cls/, '$1'));
                    }
                })
        );
    });

    await Promise.all(promises);

    // No test classes in this folder, moving on
    if (classes.length === 0) {
        return;
    }
    classes.sort();
    // If we are sharding tests then select only the specific tests we're assigned to run
    if (testNum && testNum.length > 0) {
        const runnerNum = parseInt(testNum, 10);
        const runnersCount = parseInt(getEnvVar('CI_SFDX_SB_TOTAL_ORGS'), 10);
        let newClasses = [];
        for (let i = runnerNum - 1; i < classes.length; i = i + runnersCount) {
            newClasses.push(classes[i]);
        }
        classes = newClasses;
    }

    // DX has a bug where it needs two elements in the list
    if (classes.length === 1) {
        classes.push(classes[0]);
    }

    let data;
    await sfdx(
        'force:mdapi:deploy',
        `--deploydir distDiff --testlevel RunSpecifiedTests --targetusername ${getEnvVar(
            'CI_SFDX_ORG_USERNAME'
        )} ${getEnvVar(
            'CI_SFDX_MDAPIDEPLOY_ARGS'
        )} --wait 200 --json --runtests ${classes.join(',')}`
    )
        .then((o) => {
            data = o;
        })
        .catch((o) => {
            data = o;
        });
    const j = JSON.parse(data.stdout);
    printDeployFailures(j);
    if (j.result.details.runTestResult.failures) {
        throw Error('Deployment failed with apex test failures');
    }
}

function printDeployFailures(j) {
    console.log('Deployment Status:', j.status);
    if (!j.result.details) {
        throw Error(j.message);
    }
    if (j.result.details.componentFailures) {
        console.log('Component Failures:', j.result.details.componentFailures);
        throw Error('Deployment failed with component failures');
    }
    console.log('Tests Executed:', j.result.details.runTestResult.numTestsRun);
    console.log('Tests Failed:', j.result.details.runTestResult.numFailures);
    console.log(
        'Coverage Warnings:',
        j.result.details.runTestResult.codeCoverageWarnings
    );
    console.log('Tests Failed:', j.result.details.runTestResult.failures);
}
