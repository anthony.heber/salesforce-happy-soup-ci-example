require('dotenv').config();
const sfdx = require('./sfdx1');
const USERNAME = process.env.SFDX_USERNAME_CYPRESS;
const COMM_USERNAME = process.env.SFDX_COMM_USERNAME_CYPRESS;
const SFDX_OPTIONS = { env: { FORCE_COLOR: '0' } };

// Builds a list of all the domains for your current user
// The tests choose the appropriate URL for their scope
let siteMap;
async function createCommunityBaseURL(community) {
    if (!siteMap) {
        const records = await querySFDX(
            'SELECT Site.Name, Domain.Domain, PathPrefix FROM DomainSite'
        );
        siteMap = records.reduce((urls, s) => {
            if (!urls[s.Site.Name] || !s.Domain.Domain.endsWith('.force.com')) {
                urls[s.Site.Name] = `https://${s.Domain.Domain}${s.PathPrefix}`;
            }
            return urls;
        }, {});
    }
    console.log(`Selected site url: ${siteMap[community]}`);
    return siteMap[community];
}
// Builds a url for which your SF user is already authenticated
async function buildAuthenticatedURL(community) {
    const networkObject = (
        await querySFDX(
            `SELECT Id, Name FROM Network WHERE Name = '${community}'`
        )
    )[0];
    let id = networkObject.Id.slice(0, -3);

    let userNameString = buildUserNameString(USERNAME);
    const cmd2Out = JSON.parse(
        await sfdx(
            'force:org:open',
            `${userNameString} -r --json -p "/servlet/networks/switch?networkId=${id}"`,
            SFDX_OPTIONS
        )
    );
    let communityURL = cmd2Out.result.url;
    console.log(`URL Built: ${communityURL}`);
    return communityURL;
}
// Builds a URL for which a community user in the org is already authenticated
async function createCommunityUserURL(community) {
    const promises = [];
    promises.push(
        querySFDX(`SELECT Id, Name FROM Network WHERE Name = '${community}'`)
    );
    promises.push(querySFDX('SELECT Id FROM Organization'));
    let query;
    if (COMM_USERNAME) {
        query = `SELECT Id, Username from User WHERE Username = '${COMM_USERNAME}'`;
    } else {
        query =
            "SELECT Id, Username from User WHERE IsActive = true AND UserType = 'PowerPartner' LIMIT 1";
    }
    promises.push(querySFDX(query));
    let queries = await Promise.all(promises);

    const networkObject = queries[0][0];
    let id = networkObject.Id.slice(0, -3);

    const orgObject = queries[1][0];
    let orgId = orgObject.Id.slice(0, -3);

    const user = queries[2][0];
    console.log(user.Username);
    let uid = user.Id.slice(0, -3);

    let userNameString = buildUserNameString(USERNAME);
    const cmd4Out = JSON.parse(
        await sfdx(
            'force:org:open',
            `${userNameString} -r --json -p "/servlet/servlet.su?oid=${orgId}&sunetworkid=${id}&sunetworkuserid=${uid}"`,
            SFDX_OPTIONS
        )
    );
    let communityURL = cmd4Out.result.url;
    console.log(`URL Built: ${communityURL}`);
    return communityURL;
}

module.exports = {
    createCommunityBaseURL,
    buildAuthenticatedURL,
    createCommunityUserURL
};

function buildUserNameString(userName) {
    let userNameString = '';
    if (userName) {
        userNameString = `-u ${userName}`;
    }
    return userNameString;
}

async function querySFDX(query) {
    let userNameString = buildUserNameString(USERNAME);
    let site = await sfdx(
        'force:data:soql:query',
        `${userNameString} -q "${query}" --json`,
        SFDX_OPTIONS
    );
    let output = JSON.parse(site);
    return output.result.records;
}
