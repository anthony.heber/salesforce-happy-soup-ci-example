require('dotenv').config();
const sfdx = require('./sfdx');
const SFDX_OPTIONS = { env: { FORCE_COLOR: '0' } };

// Builds a url for which your SF user is already authenticated
async function getExperienceAccessURL(username, experience) {
    const networkObject = (
        await querySFDX(
            username,
            `SELECT Id, Name FROM Network WHERE Name = '${experience}'`
        )
    )[0];
    let id = networkObject.Id.slice(0, -3);

    const cmd2Out = JSON.parse(
        (
            await sfdx(
                'force:org:open',
                `-u ${username} -r --json -p "/servlet/networks/switch?networkId=${id}"`,
                SFDX_OPTIONS
            )
        ).stdout
    );
    let experienceURL = cmd2Out.result.url;
    return experienceURL;
}

// Builds a URL for which a community user in the org is already authenticated
async function getOrgAccessUrl(username, url) {
    let pathString = '';
    if (url) {
        pathString = `-p "${url}"`;
    }
    const cmd4Out = JSON.parse(
        (
            await sfdx(
                'force:org:open',
                `-u "${username}" -r --json ${pathString}`,
                SFDX_OPTIONS
            )
        ).stdout
    );
    let communityURL = cmd4Out.result.url;
    return communityURL;
}

module.exports = {
    getExperienceAccessURL,
    getOrgAccessUrl,
    querySFDX
};

async function querySFDX(username, query) {
    let out = await sfdx(
        'force:data:soql:query',
        `-u ${username} -q "${query}" --json`,
        SFDX_OPTIONS
    );
    let output = JSON.parse(out.stdout);
    return output.result.records;
}
