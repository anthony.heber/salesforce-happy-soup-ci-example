'use strict';
require('./scriptCommon');
const sfdx = require('./sfdx');
const createScratchOrg = require('./createScratchOrg');
const { listApexTestClasses, getEnvVar } = require('./utils');

let status = 0;

async function run() {
    const getClassListPromise = listApexTestClasses({
        totalShards: process.env.CI_SFDX_SCRATCH_TOTAL_ORGS,
        shardNumber: process.env.CI_SFDX_SCRATCH_ORG_NUM
    });
    console.log('Get org from vending machine');
    // claim org from vending machine for execution

    console.log('Finished pre-build ');
    await createScratchOrg();
    const classList = await getClassListPromise;

    // print login url for the org
    await sfdx(
        'force:org:open',
        `-r --targetusername=${getEnvVar('CI_SFDX_ORG')}`
    ).then((output) => {
        console.log(output.stdout);
    });

    try {
        await sfdx(
            'force:apex:test:run',
            `--wait=90 --json --targetusername=${getEnvVar(
                'CI_SFDX_ORG'
            )} --classnames="${classList.join(',')}"`
        );
    } catch ({ stdout }) {
        status = 1;
        const results = JSON.parse(stdout);
        console.log('Result Code:', results.status);
        console.log(JSON.stringify(results.result.summary, null, 2));
        console.log(
            JSON.stringify(
                results.result.tests.filter((t) => t.Outcome !== 'Pass'),
                null,
                2
            )
        );
    }

    console.log('Queueing org for deletion');
    sfdx('force:org:delete', '-u scratch -p');
}

run()
    .catch((err) => {
        status = 1;
        console.log('Error:', err);
    })
    .finally(() => {
        process.exit(status);
    });
